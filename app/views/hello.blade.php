<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			width: 300px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -150px;
			margin-top: -100px;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="{{URL::to('/js/tmpl.min.js')}}"></script>
    <script src="{{URL::to('/js/load-image.all.min.js')}}"></script>
    <script src="{{URL::to('/js/canvas-to-blob.min.js')}}"></script>
    <script src="{{URL::to('/js/blueimp-gallery.min.js')}}"></script>
    <script src="{{URL::to('/js/jquery.iframe-transport.js')}}"></script>
    <script src="{{URL::to('/js/jquery.fileupload.js')}}"></script>
    <script src="{{URL::to('/js/jquery.fileupload-process.js')}}"></script>
    <script src="{{URL::to('/js/jquery.fileupload-image.js')}}"></script>
    <script src="{{URL::to('/js/jquery.fileupload-validate.js')}}"></script>
    <script src="{{URL::to('/js/jquery.fileupload-ui.js')}}"></script>
</head>
<body>
	<div class="panel panel-simple marginbottom0">
    <div class="panel-heading success">
        <h3 class="panel-title"><i class="fa fa-picture-o"></i> Đăng ảnh <span class="panel-under"></span></h3>
    </div>
    <div class="panel-body">
        <div style="margin-left: 0px;display:none" class="errorMessage" id="Post_image_em_"></div>
        <div class="row fileupload-buttonbar marginbottom10">
            <div class="col-sm-13 col-xs-14">
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <span class="btn btn-success fileinput-button">
                            <span>
                                <i class="fa fa-upload"></i> Tải ảnh lên
                            </span>
                            <input type="file" name="fileupload[]" multiple>
                        </span>
                    </div>
                    <div class="col-md-16 hidden-sm hidden-xs">
                        Hoặc kéo & thả ảnh vào đây
                    </div>
                </div>
            </div>
            <div class="col-sm-11 col-xs-10 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" style="margin: 0;" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="bar progress-bar" style="width:0%;">
                    </div>
                </div>
                <!-- The extended global progress information -->
            </div>
        </div>
        <div class="visible-sm visible-xs text-small text-muted">
            Nếu thiết bị của bạn upload ảnh bị lỗi, hãy giải phóng RAM và up từng cái một.
        </div>
        <!-- The table listing the files available for upload/download -->
        <div id="upload-grid">
            <table role="presentation" class="table table-striped table-hover table-condensed">
                <tbody class="files">
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a> <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<link rel="stylesheet" href="{{URL::to('/css/blueimp-gallery.min.css')}}">
<link rel="stylesheet" href="{{URL::to('/css/jquery.fileupload.css')}}">
<link rel="stylesheet" href="{{URL::to('/css/jquery.fileupload-ui.css')}}">
<noscript>
    <link rel="stylesheet" href="{{URL::to('/css/jquery.fileupload-noscript.css')}}">
</noscript>
<noscript>
    <link rel="stylesheet" href="{{URL::to('/css/jquery.fileupload-ui-noscript.css')}}">
</noscript>
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td></td>
        <td> <span class="preview"></span> </td>
        <td>
            <strong class="error text-danger"></strong>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
            <button class="btn btn-primary btn-small start" disabled><i class="fa fa-upload"></i> <span>Upload</span></button>
            {% } %}
            {% if (!i) { %}
            <button class="btn btn-warning btn-small cancel"><i class="fa fa-ban"></i> <span>Hủy</span></button>
            {% } %}
        </td>
    </tr>
    {% } %}
</script>
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.url_thumb) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery>
                    <img src="{%=file.url_thumb%}">
                </a>
                {% } %}
            </span>
        </td>
        <td>
            {% if (!file.error) { %}
            <input class="image_cover" {% if (file.is_cover == 1) { %} checked = "checked" {% }%} type="radio" name="Post[cover]" value="{%=file.id%}" rel="tooltip" title="Chọn làm ảnh đại diện" />
            {% } %}
        </td>
        <td>
            {% if (!file.error) { %}
            <p class="name">
                <a href="" title="Chèn ảnh này vào bài viết" url="{%=file.url%}" class="add_to_content btn btn-small btn-primary">
                    <i class="fa fa-external-link"></i> Chèn vào bài
                </a>
            </p>
            {% } %}
            {% if (file.error) { %}
            <div>
                <span class="label label-danger">Error </span>
                {%=file.error%}
            </div>
            {% } %}
        </td>
        <td>
            <button class="btn btn-danger btn-small delete" data-type="{%=file.delete_method%}" data-url="{%=file.delete_url%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}> <i class="fa fa-trash-o"></i> <span>Xóa</span> </button>
            <input type="hidden" name="Post[images][]" value="{%=file.name%}"
        </td>
    </tr> 
{% } %}
</script>

<script>
    var upload_url='{{route('upload')}}';
</script>
</body>
</html>
